import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    `java-gradle-plugin`
    java
    application
    id("com.github.johnrengelman.shadow") version "4.0.3"
}

repositories {
    jcenter()
    mavenCentral()
}

val vertxVersion = "3.7.0"
val vertxGuiceVersion = "2.3.1"
val reactorVersion = "3.2.8.RELEASE"
val rxJavaVersion = "2.2.8"
val lombokVersion = "1.18.6"
val hsqlVersion = "2.4.1"
val slf4jVersion = "1.7.26"
val mockitoVersion = "2.27.0"
val assertJVersion = "3.12.2"
val junitVersion = "5.3.2"

dependencies {
    annotationProcessor("org.projectlombok:lombok:$lombokVersion")
    compileOnly("org.projectlombok:lombok:$lombokVersion")
    implementation("io.vertx:vertx-core:$vertxVersion")
    implementation("io.vertx:vertx-web:$vertxVersion")
    implementation("io.vertx:vertx-jdbc-client:$vertxVersion")
    implementation("io.vertx:vertx-rx-java2:$vertxVersion")
    implementation("com.englishtown.vertx:vertx-guice:$vertxGuiceVersion")
    implementation("io.projectreactor:reactor-core:$reactorVersion")
    implementation("io.reactivex.rxjava2:rxjava:$rxJavaVersion")
    implementation("org.projectlombok:lombok:$lombokVersion")
    implementation("org.hsqldb:hsqldb:$hsqlVersion")
    implementation("org.slf4j:slf4j-simple:$slf4jVersion")
    annotationProcessor("org.apache.logging.log4j:log4j-core:2.11.1")

    
    testImplementation("io.vertx:vertx-junit5:$vertxVersion")
    testImplementation("io.vertx:vertx-web-client:$vertxVersion")
    testCompile("org.assertj:assertj-core:$assertJVersion")
    testCompile("org.mockito:mockito-junit-jupiter:$mockitoVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
}

application {
    mainClassName = "io.vertx.core.Launcher"
}

val mainVerticleName = "com.revolut.MainVerticle"
val watchForChange = "src/**/*.java"
val doOnChange = "${projectDir}/gradlew classes"

tasks {
    test {
        useJUnitPlatform()
    }

    getByName<JavaExec>("run") {
        args = listOf("run", mainVerticleName, "--redeploy=${watchForChange}", "--launcher-class=${application.mainClassName}", "--on-redeploy=${doOnChange}")
    }

    withType<ShadowJar> {
        classifier = "fat"
        manifest {
            attributes["Main-Verticle"] = mainVerticleName
        }
        mergeServiceFiles {
            include("META-INF/services/io.vertx.core.spi.VerticleFactory")
        }
    }
}