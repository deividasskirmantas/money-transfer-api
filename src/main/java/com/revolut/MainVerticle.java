package com.revolut;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.api.ApiVerticle;
import com.revolut.transaction.TransactionVerticle;
import io.reactivex.Single;
import io.vertx.core.Future;
import io.vertx.core.Launcher;
import io.vertx.reactivex.core.AbstractVerticle;

public class MainVerticle extends AbstractVerticle {

    public static void main(String[] args) {
        Launcher.executeCommand("run", MainVerticle.class.getName());
    }

    @Override
    public void start(Future<Void> startFuture) {
        Injector injector = Guice.createInjector(new InjectionBinder(vertx));

        Single<String> transactionVerticleDeployment = vertx.rxDeployVerticle(injector.getInstance(TransactionVerticle.class));
        transactionVerticleDeployment
                .flatMap(id -> vertx.rxDeployVerticle(injector.getInstance(ApiVerticle.class)))
                .subscribe(id -> startFuture.complete(), startFuture::fail);
    }
}