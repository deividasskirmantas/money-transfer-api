package com.revolut.api;

import com.google.inject.Inject;
import com.revolut.db.ApiRepository;
import com.revolut.db.HSQLServer;
import com.revolut.model.AccountMapper;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.eventbus.Message;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.handler.BodyHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.function.Function;

@Slf4j
public class ApiVerticle extends AbstractVerticle {

    public static final String TRANSACTION_QUEUE = "transaction.queue";

    @Inject
    private ApiRepository repository;
    @Inject
    private AccountMapper accountMapper;
    @Inject
    private HSQLServer server;

    @Override
    public void start(Future<Void> startFuture) {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());

        router.get("/api/accounts").handler(this::getAccounts);
        router.get("/api/accounts/:accountId").handler(this::getAccount);
        router.get("/api/accounts/:accountId/transactions").handler(this::getAccountTransactions);
        router.get("/api/transactions").handler(this::getTranasctions);
        router.get("/api/transactions/:transactionId").handler(this::getTranasction);
        router.post("/api/accounts").handler(this::createAccount);
        router.post("/api/transactions").handler(this::createTransaction);

        vertx.createHttpServer()
             .requestHandler(router)
             .rxListen(8080)
             .subscribe(s -> startFuture.complete(),
                        startFuture::fail);
    }

    @Override
    public void stop(Future<Void> stopFuture) {
        server.shutdownServer();
        stopFuture.complete();
    }

    private void getAccounts(RoutingContext context) {
        repository.fetchAllAccounts()
                  .subscribe(accounts -> successResponse(context, "accounts", accounts),
                             throwable -> errorResponse(context, throwable));
    }

    private void getAccount(RoutingContext context) {
        long accountId = Long.valueOf(context.request().getParam("accountId"));
        repository.fetchAccountBy(accountId)
                  .subscribe(account -> successResponse(context, "account", Collections.singletonList(account)),
                             throwable -> errorResponse(context, throwable));
    }

    private void getAccountTransactions(RoutingContext context) {
        long accountId = Long.valueOf(context.request().getParam("accountId"));
        repository.fetchAccountTransactions(accountId)
                  .subscribe(transactions -> successResponse(context, "transactions", transactions),
                             throwable -> errorResponse(context, throwable));
    }

    private void getTranasctions(RoutingContext context) {
        repository.fetchAllTranasctions()
                  .subscribe(transactions -> successResponse(context, "transactions", transactions),
                             throwable -> errorResponse(context, throwable));
    }

    private void getTranasction(RoutingContext context) {
        long transactionId = Long.valueOf(context.request().getParam("transactionId"));
        repository.fetchTranasctionBy(transactionId)
                  .subscribe(transaction -> successResponse(context, "transaction", Collections.singletonList(transaction)),
                             throwable -> errorResponse(context, throwable));
    }

    private void createAccount(RoutingContext context) {
        JsonObject accountJson = context.getBodyAsJson();
        repository.createAccount(accountMapper.fromHTTP(accountJson))
                  .subscribe(() -> successResponse(context),
                             throwable -> errorResponse(context, throwable));
    }

    private void createTransaction(RoutingContext context) {
        JsonObject transactionJson = context.getBodyAsJson();

        vertx.eventBus().send(TRANSACTION_QUEUE, transactionJson,
                              reply -> reply.map(successfullTransaction(context))
                                            .otherwise(failedTransaction(context))
                                            .result().run());
    }

    private Function<Message<Object>, Runnable> successfullTransaction(RoutingContext context) {
        return message -> (Runnable) () -> successResponse(context);
    }

    private Function<Throwable, Runnable> failedTransaction(RoutingContext context) {
        return exception -> (Runnable) () -> errorResponse(context, exception);
    }

    private void successResponse(RoutingContext context) {
        successResponse(context, null, null);
    }

    private void successResponse(RoutingContext context, String fieldName, Object data) {
        context.response().setStatusCode(200);
        context.response().putHeader("Content-Type", "application/json");
        JsonObject responseBody = new JsonObject().put("success", true);
        if (fieldName != null && data != null) {
            responseBody.put(fieldName, data);
        }
        context.response().end(responseBody.encode());
    }

    private void errorResponse(RoutingContext context, Throwable t) {
        context.response().setStatusCode(500);
        context.response().putHeader("Content-Type", "application/json");
        context.response().end(new JsonObject()
                                       .put("success", false)
                                       .put("error", t.getMessage()).encode());
    }
}