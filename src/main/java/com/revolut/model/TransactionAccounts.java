package com.revolut.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TransactionAccounts {
    private Account accountFrom;
    private Account accountTo;
}