package com.revolut.model;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class AccountMapper {

    public Account fromDB(JsonObject json) {
        JsonObjectWrapper wrapper = JsonObjectWrapper.of(json);

        return fromHTTP(json).toBuilder()
                             .id(wrapper.getLong("ID"))
                             .build();
    }

    public Account fromHTTP(JsonObject json) {
        JsonObjectWrapper wrapper = JsonObjectWrapper.of(json);

        return Account.builder()
                      .name(wrapper.getString("NAME"))
                      .balance(wrapper.getBigDecimal("BALANCE"))
                      .build();
    }

    public JsonArray toJsonArray(Account account) {
        return new JsonArray().add(account.getName())
                              .add(account.getBalance().doubleValue());
    }
}