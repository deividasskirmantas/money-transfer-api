package com.revolut.model;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder(toBuilder = true)
public class Account {
    private Long id;
    private String name;
    private BigDecimal balance;
}