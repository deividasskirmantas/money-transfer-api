package com.revolut.model;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Value
@Builder(toBuilder = true)
public class Transaction {
    private Long id;
    private Long accountFrom;
    private Long accountTo;
    private BigDecimal amount;
    private LocalDateTime created;
}