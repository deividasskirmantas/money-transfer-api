package com.revolut.model;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class TransactionMapper {

    public Transaction fromDB(JsonObject json) {
        JsonObjectWrapper wrapper = JsonObjectWrapper.of(json);

        return fromHTTP(json).toBuilder()
                             .id(wrapper.getLong("ID"))
                             .created(wrapper.getLocalDateTime("CREATED"))
                             .build();
    }

    public Transaction fromHTTP(JsonObject json) {
        JsonObjectWrapper wrapper = JsonObjectWrapper.of(json);

        return Transaction.builder()
                          .accountFrom(wrapper.getLong("ACCOUNTFROM"))
                          .accountTo(wrapper.getLong("ACCOUNTTO"))
                          .amount(wrapper.getBigDecimal("AMOUNT"))
                          .build();
    }

    public JsonArray toJsonArray(Transaction transaction) {
        return new JsonArray().add(transaction.getAccountFrom())
                              .add(transaction.getAccountTo())
                              .add(transaction.getAmount().doubleValue());
    }
}