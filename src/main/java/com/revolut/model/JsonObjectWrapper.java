package com.revolut.model;

import io.vertx.core.json.JsonObject;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.Optional;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;

public class JsonObjectWrapper {

    private Map<String, Object> jsonMap;

    private JsonObjectWrapper(JsonObject json) {
        this.jsonMap = json.getMap();
    }

    public static JsonObjectWrapper of(JsonObject json) {
        return new JsonObjectWrapper(json);
    }

    public Long getLong(String key) {
        return getNumber(key)
                .map(Number::longValue)
                .orElse(null);
    }

    public String getString(String key) {
        return (String) get(key).orElse(null);
    }

    public BigDecimal getBigDecimal(String key) {
        return getNumber(key)
                .map(Number::doubleValue)
                .map(BigDecimal::valueOf)
                .orElse(null);
    }

    public LocalDateTime getLocalDateTime(String key) {
        return get(key).map(o -> (String) o)
                       .map(ISO_INSTANT::parse)
                       .map(Instant::from)
                       .map(inst -> inst.atZone(ZoneId.systemDefault()).toLocalDateTime())
                       .orElse(null);
    }

    private Optional<Object> get(String key) {
        return jsonMap.entrySet()
                      .stream()
                      .filter(jsonEntry -> jsonEntry.getKey().toUpperCase().equals(key))
                      .findFirst()
                      .map(Map.Entry::getValue);
    }

    private Optional<Number> getNumber(String key) {
        return get(key).map(o -> (Number) o);
    }
}