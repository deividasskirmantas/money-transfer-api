package com.revolut;

import com.google.inject.AbstractModule;
import com.revolut.api.ApiVerticle;
import com.revolut.db.ApiRepository;
import com.revolut.db.HSQLServer;
import com.revolut.db.TransactionRepository;
import com.revolut.model.AccountMapper;
import com.revolut.model.TransactionMapper;
import com.revolut.transaction.TransactionVerticle;
import io.vertx.reactivex.core.Vertx;

public class InjectionBinder extends AbstractModule {

    private Vertx vertx;

    public InjectionBinder(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    protected void configure() {
        bind(ApiVerticle.class).toInstance(new ApiVerticle());
        bind(TransactionVerticle.class).toInstance(new TransactionVerticle());

        bind(HSQLServer.class).toInstance(new HSQLServer());
        bind(ApiRepository.class).toInstance(new ApiRepository(vertx));
        bind(TransactionRepository.class).toInstance(new TransactionRepository(vertx));
        bind(AccountMapper.class).toInstance(new AccountMapper());
        bind(TransactionMapper.class).toInstance(new TransactionMapper());
    }
}