package com.revolut.transaction;

import com.google.inject.Inject;
import com.revolut.db.HSQLServer;
import com.revolut.db.TransactionRepository;
import com.revolut.model.Transaction;
import com.revolut.model.TransactionAccounts;
import com.revolut.model.TransactionMapper;
import io.reactivex.functions.Consumer;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.eventbus.Message;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Predicate;

import static com.revolut.api.ApiVerticle.TRANSACTION_QUEUE;

@Slf4j
public class TransactionVerticle extends AbstractVerticle {

    @Inject
    private TransactionRepository repository;
    @Inject
    private TransactionMapper transactionMapper;

    @Override
    public void start(Future<Void> startFuture) {
        vertx.eventBus().consumer(TRANSACTION_QUEUE, this::createTransaction);
        startFuture.complete();
    }

    private void createTransaction(Message<JsonObject> message) {
        if (message.body() instanceof  JsonObject) {
            JsonObject transactionJson = message.body();
            Transaction newTransaction = transactionMapper.fromHTTP(transactionJson);
            if (validateTransaction(newTransaction, message)) {
                repository.fetchTransactionAccounts(newTransaction.getAccountFrom(),
                                                    newTransaction.getAccountTo())
                          .subscribe(checkDebitAndPersist(message, newTransaction),
                                     throwable -> message.fail(0, throwable.getMessage()));
            }
        } else {
            message.fail(0, "Transaction message should arrive in JSON format");
        }
    }

    private Consumer<TransactionAccounts> checkDebitAndPersist(Message<JsonObject> message, Transaction newTransaction) {
        return transactionAccounts -> {
            if (isDebitPossible(newTransaction).test(transactionAccounts)) {
                repository.createTransaction(newTransaction, transactionAccounts)
                          .subscribe(() -> message.reply(new JsonObject()),
                                     throwable -> message.fail(0, throwable.getMessage()));
            } else {
                message.fail(0, "Insufficient funds in AccountFrom balance");
            }
        };
    }

    private Predicate<TransactionAccounts> isDebitPossible(Transaction newTransaction) {
        return ta -> ta.getAccountFrom() != null && ta.getAccountFrom().getBalance() != null && newTransaction.getAmount() != null &&
                ta.getAccountFrom().getBalance().compareTo(newTransaction.getAmount()) >= 0;
    }

    private boolean validateTransaction(Transaction transaction, Message<JsonObject> message) {
        if (transaction.getAccountFrom() == null) {
            message.fail(0, "AccountFrom is mandatory field in transaction");
            return false;
        }
        if (transaction.getAccountTo() == null) {
            message.fail(0, "AccountTo is mandatory field in transaction");
            return false;
        }
        if (transaction.getAmount() == null) {
            message.fail(0, "Amount is mandatory field in transaction");
            return false;
        } else if (transaction.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            message.fail(0, "Amount should be above zero");
            return false;
        }
        return true;
    }
}