package com.revolut.db;

import com.google.inject.Inject;
import com.revolut.model.Account;
import com.revolut.model.AccountMapper;
import com.revolut.model.Transaction;
import com.revolut.model.TransactionAccounts;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.Vertx;
import lombok.SneakyThrows;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.revolut.db.HSQLServer.DB_URL;
import static com.revolut.db.HSQLServer.DB_USERNAME;

public class TransactionRepository {

    private static final String FETCH_TRANSACTION_ACCOUNTS = "SELECT * FROM account WHERE id = ? OR id = ?";
    private static final String INSERT_TRANSACTION = "INSERT INTO transaction (accountFrom, accountTo, amount) VALUES (?, ?, ?)";
    private static final String UPDATE_ACCOUNT_BALANCE = "UPDATE account SET balance = ? WHERE id = ?";

    @Inject
    private AccountMapper accountMapper;

    private Connection conn;

    @Inject
    @SneakyThrows
    public TransactionRepository(Vertx vertx) {
        conn = DriverManager.getConnection(DB_URL, DB_USERNAME, "");
    }

    @SneakyThrows
    public Single<TransactionAccounts> fetchTransactionAccounts(Long accountFromId, Long accountToId) {
        Single<TransactionAccounts> result = null;
        try {
            PreparedStatement ps1 = conn.prepareStatement(FETCH_TRANSACTION_ACCOUNTS);
            ps1.setLong(1, accountFromId);
            ps1.setLong(2, accountToId);
            java.sql.ResultSet resultSet = ps1.executeQuery();

            List<Account> accounts = new ArrayList<>();
            while (resultSet.next()) {
                accounts.add(accountMapper.fromDB(new JsonObject()
                                                          .put("id", resultSet.getLong("id"))
                                                          .put("name", resultSet.getString("name"))
                                                          .put("balance", resultSet.getDouble("balance"))));
            }

            if (accounts.size() != 2) {
                throw new RuntimeException("Transaction can not be processed because account(-s) do not exist in the system");
            }
            result = Single.just(TransactionAccounts.builder()
                                                    .accountFrom(getAccount(accounts, accountFromId))
                                                    .accountTo(getAccount(accounts, accountToId))
                                                    .build());
        } catch (Exception e) {
            conn.rollback();
            result = Single.error(e);
        }
        return result;
    }

    private Account getAccount(List<Account> dbAccounts, Long accountId) {
        return dbAccounts.stream()
                         .filter(acc -> Objects.equals(acc.getId(), accountId))
                         .findFirst()
                         .orElseThrow(() -> new RuntimeException("Transaction can not be processed due to database error"));
    }

    @SneakyThrows
    public Completable createTransaction(Transaction transaction, TransactionAccounts accounts) {
        Completable result = null;
        conn.setAutoCommit(false);
        try {
            PreparedStatement ps1 = conn.prepareStatement(INSERT_TRANSACTION);
            ps1.setLong(1, transaction.getAccountFrom());
            ps1.setLong(2, transaction.getAccountTo());
            ps1.setDouble(3, transaction.getAmount().doubleValue());
            ps1.executeUpdate();

            Account accountFrom = accounts.getAccountFrom();
            updateAccountBalance(accountFrom.getId(), accountFrom.getBalance().subtract(transaction.getAmount()).doubleValue());
            Account accountTo = accounts.getAccountTo();
            updateAccountBalance(accountTo.getId(), accountTo.getBalance().add(transaction.getAmount()).doubleValue());

            conn.commit();
            result = Completable.complete();
        } catch (Exception e) {
            conn.rollback();
            result = Completable.error(e);
        }
        return result;
    }

    private void updateAccountBalance(Long accountId, Double amount) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(UPDATE_ACCOUNT_BALANCE);
        ps.setDouble(1, amount);
        ps.setLong(2, accountId);
        ps.executeUpdate();
    }
}