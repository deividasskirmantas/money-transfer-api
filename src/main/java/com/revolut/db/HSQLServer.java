package com.revolut.db;

import org.hsqldb.Database;
import org.hsqldb.server.Server;
import org.hsqldb.server.ServerConstants;

public class HSQLServer {

    public static final String DB_DRIVER = "org.hsqldb.jdbcDriver";
    public static final String DB_URL = "jdbc:hsqldb:mem:revolutdb";
    public static final String DB_USERNAME = "SA";

    private Server server;

    public HSQLServer() {
        server = new Server();
        server.setDatabaseName(0, "revolutdb");
        server.setDatabasePath(0, "mem:revolutdb");
        server.setSilent(true);
        server.start();
    }

    public void shutdownServer() {
        if (server.getState() == ServerConstants.SERVER_STATE_ONLINE) {
            server.signalCloseAllServerConnections();
            server.shutdownWithCatalogs(Database.CLOSEMODE_IMMEDIATELY);
            server.shutdown();
        }
    }
}