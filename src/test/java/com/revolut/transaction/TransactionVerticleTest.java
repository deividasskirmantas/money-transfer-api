package com.revolut.transaction;

import com.revolut.db.TransactionRepository;
import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.model.TransactionAccounts;
import com.revolut.model.TransactionMapper;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.reactivex.core.Vertx;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static com.revolut.api.ApiVerticle.TRANSACTION_QUEUE;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;

@ExtendWith(value = {VertxExtension.class, MockitoExtension.class})
class TransactionVerticleTest {

    @Mock
    private TransactionRepository repository;
    @Mock
    private TransactionMapper transactionMapper;
    @InjectMocks
    private TransactionVerticle transactionVerticle;

    @BeforeEach
    void init(Vertx vertx, VertxTestContext testContext) {
        vertx.deployVerticle(transactionVerticle, testContext.completing());
    }

    @Test
    void eventMessageShouldBeJSON_Success(Vertx vertx, VertxTestContext testContext) {
        //given
        given(transactionMapper.fromHTTP(any(JsonObject.class))).willReturn(Transaction.builder().build());

        //when
        vertx.eventBus().send(TRANSACTION_QUEUE, new JsonObject(), reply -> {
                                  //then
                                  then(transactionMapper).should().fromHTTP(any(JsonObject.class));
                                  testContext.completeNow();
                              }
        );
    }

    @Test
    void eventMessageShouldBeJSON_Failure(Vertx vertx, VertxTestContext testContext) {
        //when
        vertx.eventBus().send(TRANSACTION_QUEUE, "Not JSON", reply -> {
                                  //then
                                  assertThat(reply.failed()).isTrue();
                                  assertThat(reply.cause()).hasMessage("Transaction message should arrive in JSON format");
                                  then(transactionMapper).should(never()).fromHTTP(any(JsonObject.class));
                                  testContext.completeNow();
                              }
        );
    }

    @Test
    void mandatoryAccountFromInTransaction(Vertx vertx, VertxTestContext testContext) {
        //given
        given(transactionMapper.fromHTTP(any(JsonObject.class))).willReturn(Transaction.builder().build());

        //when
        vertx.eventBus().send(TRANSACTION_QUEUE, new JsonObject(), reply -> {
                                  //then
                                  assertThat(reply.failed()).isTrue();
                                  assertThat(reply.cause()).hasMessage("AccountFrom is mandatory field in transaction");
                                  testContext.completeNow();
                              }
        );
    }

    @Test
    void mandatoryAccountToInTransaction(Vertx vertx, VertxTestContext testContext) {
        //given
        given(transactionMapper.fromHTTP(any(JsonObject.class))).willReturn(Transaction.builder()
                                                                                       .accountFrom(1L)
                                                                                       .build());

        //when
        vertx.eventBus().send(TRANSACTION_QUEUE, new JsonObject(), reply -> {
                                  //then
                                  assertThat(reply.failed()).isTrue();
                                  assertThat(reply.cause()).hasMessage("AccountTo is mandatory field in transaction");
                                  then(repository).should(never()).fetchTransactionAccounts(anyLong(), anyLong());
                                  testContext.completeNow();
                              }
        );
    }

    @Test
    void mandatoryAmountInTransaction(Vertx vertx, VertxTestContext testContext) {
        //given
        given(transactionMapper.fromHTTP(any(JsonObject.class))).willReturn(Transaction.builder()
                                                                                       .accountFrom(1L)
                                                                                       .accountTo(2L)
                                                                                       .build());

        //when
        vertx.eventBus().send(TRANSACTION_QUEUE, new JsonObject(), reply -> {
                                  //then
                                  assertThat(reply.failed()).isTrue();
                                  assertThat(reply.cause()).hasMessage("Amount is mandatory field in transaction");
                                  then(repository).should(never()).fetchTransactionAccounts(anyLong(), anyLong());
                                  testContext.completeNow();
                              }
        );
    }

    @Test
    void negativeAmountIsNotAllowedInTransaction(Vertx vertx, VertxTestContext testContext) {
        //given
        given(transactionMapper.fromHTTP(any(JsonObject.class))).willReturn(Transaction.builder()
                                                                                       .accountFrom(1L)
                                                                                       .accountTo(2L)
                                                                                       .amount(BigDecimal.valueOf(-5L))
                                                                                       .build());

        //when
        vertx.eventBus().send(TRANSACTION_QUEUE, new JsonObject(), reply -> {
                                  //then
                                  assertThat(reply.failed()).isTrue();
                                  assertThat(reply.cause()).hasMessage("Amount should be above zero");
                                  then(repository).should(never()).fetchTransactionAccounts(anyLong(), anyLong());
                                  testContext.completeNow();
                              }
        );
    }

    @Test
    void transactionValidationPasses(Vertx vertx, VertxTestContext testContext) {
        //given
        given(transactionMapper.fromHTTP(any(JsonObject.class))).willReturn(Transaction.builder()
                                                                                       .accountFrom(1L)
                                                                                       .accountTo(2L)
                                                                                       .amount(BigDecimal.valueOf(3L))
                                                                                       .build());
        given(repository.fetchTransactionAccounts(anyLong(), anyLong())).willReturn(Single.just(TransactionAccounts.builder().build()));

        //when
        vertx.eventBus().send(TRANSACTION_QUEUE, new JsonObject(), reply -> {
                                  //then
                                  then(repository).should().fetchTransactionAccounts(1L, 2L);
                                  testContext.completeNow();
                              }
        );
    }

    @Test
    void checkForInsufficientFunds_Failure(Vertx vertx, VertxTestContext testContext) {
        //given
        Transaction transaction = Transaction.builder()
                                             .accountFrom(1L)
                                             .accountTo(2L)
                                             .amount(BigDecimal.valueOf(11L))
                                             .build();
        TransactionAccounts transactionAccounts = TransactionAccounts.builder()
                                                                     .accountFrom(Account.builder()
                                                                                         .balance(BigDecimal.TEN)
                                                                                         .build())
                                                                     .build();
        given(transactionMapper.fromHTTP(any(JsonObject.class))).willReturn(transaction);
        given(repository.fetchTransactionAccounts(anyLong(), anyLong())).willReturn(Single.just(transactionAccounts));

        //when
        vertx.eventBus().send(TRANSACTION_QUEUE, new JsonObject(), reply -> {
                                  //then
                                  assertThat(reply.failed()).isTrue();
                                  assertThat(reply.cause()).hasMessage("Insufficient funds in AccountFrom balance");
                                  then(repository).should(never()).createTransaction(any(), any());
                                  testContext.completeNow();
                              }
        );
    }

    @Test
    void checkForInsufficientFunds_Success(Vertx vertx, VertxTestContext testContext) {
        //given
        Transaction transaction = Transaction.builder()
                                             .accountFrom(1L)
                                             .accountTo(2L)
                                             .amount(BigDecimal.TEN)
                                             .build();
        TransactionAccounts transactionAccounts = TransactionAccounts.builder()
                                                                     .accountFrom(Account.builder()
                                                                                         .balance(BigDecimal.TEN)
                                                                                         .build())
                                                                     .build();
        given(transactionMapper.fromHTTP(any(JsonObject.class))).willReturn(transaction);
        given(repository.fetchTransactionAccounts(anyLong(), anyLong())).willReturn(Single.just(transactionAccounts));
        given(repository.createTransaction(any(Transaction.class), any(TransactionAccounts.class))).willReturn(Completable.complete());

        //when
        vertx.eventBus().send(TRANSACTION_QUEUE, new JsonObject(), reply -> {
                                  //then
                                  assertThat(reply.succeeded()).isTrue();
                                  then(repository).should().createTransaction(transaction, transactionAccounts);
                                  testContext.completeNow();
                              }
        );
    }

}