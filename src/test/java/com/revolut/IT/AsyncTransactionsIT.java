package com.revolut.IT;


import com.revolut.MainVerticle;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.functions.Action;
import io.vertx.core.Handler;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.core.buffer.Buffer;
import io.vertx.reactivex.ext.web.client.HttpResponse;
import io.vertx.reactivex.ext.web.client.WebClient;
import io.vertx.reactivex.ext.web.codec.BodyCodec;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import static com.revolut.IT.IntegrationTestUtils.createTestAccounts;
import static com.revolut.IT.IntegrationTestUtils.newTx;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(VertxExtension.class)
class AsyncTransactionsIT {

    @BeforeEach
    void deployMainVerticle(Vertx vertx, VertxTestContext testContext) {
        vertx.deployVerticle(new MainVerticle(), testContext.completing());
    }

    @AfterEach
    void stopMainVerticle(Vertx vertx, VertxTestContext testContext) {
        vertx.deploymentIDs().stream()
             .map(vertx::rxUndeploy)
             .forEach(Completable::blockingAwait);
        vertx.close(testContext.completing());
    }

    /**
     * Description:
     * Create a high load of asynchronous transactions which transfers money between accounts in a circle
     * acc1 -> acc2 -> acc3 -> acc1 and acc3 -> acc2 -> acc1 -> acc3
     * Each arrow visualises a transaction between accounts of small constant amount i.e. 1
     * <p>
     * Expected result:
     * After all transactions have been executed
     * 1. All three accounts should have the same balance as it was before the test
     * 2. All requested transactions have been persisted to DB
     */
    @Test
    @DisplayName("Account balances should remain consistent after high load of asynchronous transactions")
    void highLoadOfAsynchronousTransactions(Vertx vertx, VertxTestContext testContext) {
        //then
        Handler<HttpResponse<String>> verifyTransactionExists = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("\"success\":true");
        });
        Handler<HttpResponse<String>> verifyTransactionIsMissing = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("\"success\":false");
            testContext.completeNow();
        });
        Handler<HttpResponse<String>> verifyBalances = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("\"id\":1,\"name\":\"acc1\",\"balance\":100.0");
            assertThat(response.body()).contains("\"id\":2,\"name\":\"acc2\",\"balance\":100.0");
            assertThat(response.body()).contains("\"id\":3,\"name\":\"acc3\",\"balance\":100.0");
        });

        //when
        WebClient client = WebClient.create(vertx);
        int iterationCount = 1_000;
        AtomicInteger iterationIndex = new AtomicInteger(1);

        Consumer<Boolean> checkTransactionCount = (bool) -> {
            client.get(8080, "localhost", "/api/transactions/" + iterationCount * 6)
                  .as(BodyCodec.string())
                  .send(testContext.succeeding(verifyTransactionExists));
            client.get(8080, "localhost", "/api/transactions/" + iterationCount * 6 + 1)
                  .as(BodyCodec.string())
                  .send(testContext.succeeding(verifyTransactionIsMissing));
        };
        Consumer<Boolean> checkAccountBalances = (bool) ->
                client.get(8080, "localhost", "/api/accounts")
                      .as(BodyCodec.string())
                      .send(testContext.succeeding(verifyBalances));
        Action ifFinalIteration = () ->
                Optional.of(iterationIndex.getAndIncrement() == iterationCount)
                        .filter(Boolean::booleanValue)
                        .ifPresent(checkAccountBalances.andThen(checkTransactionCount));

        VertxTestContext.ExecutionBlock createTransactions = () -> {
            for (int i = 0; i < iterationCount; i++) {
                Flowable.mergeArray(client.post(8080, "localhost", "/api/transactions")
                                          .rxSendJsonObject(newTx(1, 2, 1))
                                          .toFlowable(),
                                    client.post(8080, "localhost", "/api/transactions")
                                          .rxSendJsonObject(newTx(2, 3, 1))
                                          .toFlowable(),
                                    client.post(8080, "localhost", "/api/transactions")
                                          .rxSendJsonObject(newTx(3, 1, 1))
                                          .toFlowable(),
                                    client.post(8080, "localhost", "/api/transactions")
                                          .rxSendJsonObject(newTx(3, 2, 1))
                                          .toFlowable(),
                                    client.post(8080, "localhost", "/api/transactions")
                                          .rxSendJsonObject(newTx(2, 1, 1))
                                          .toFlowable(),
                                    client.post(8080, "localhost", "/api/transactions")
                                          .rxSendJsonObject(newTx(1, 3, 1))
                                          .toFlowable())
                        .doOnComplete(ifFinalIteration)
                        .subscribe();
            }
        };

        //given
        createTestAccounts(client, testContext, createTransactions);
    }

    /**
     * Description:
     * It should not be allowed to transfer same unit of balance with asynchronous transactions
     * i.e.
     * acc1 balance = 1
     * acc2 balance = 0
     * If multiple asynchronous transactions (acc1 1 -> acc2) were created at the same time
     * then there is a risk that acc2 balance could be more than 1
     * <p>
     * Expected result:
     * After all transactions have been executed
     * 1. First account has the balance of 0, second account has the balance of 200
     * 2. Only first 10 transactions are persisted to DB
     */
    @RepeatedTest(100)
    @DisplayName("Multiple balances should not be created for the same unit of balance using asynchronous transactions")
    void shouldNotCreateMultipleTransactionsForTheSameUnitOfBalance(Vertx vertx, VertxTestContext testContext) {
        //then
        Handler<HttpResponse<String>> verifyTransactionExists = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("\"success\":true");
        });
        Handler<HttpResponse<String>> verifyTransactionIsMissing = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("\"success\":false");
            testContext.completeNow();
        });
        Handler<HttpResponse<String>> verifyBalances = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("\"id\":1,\"name\":\"acc1\",\"balance\":0.0");
            assertThat(response.body()).contains("\"id\":2,\"name\":\"acc2\",\"balance\":200.0");
        });

        //when
        WebClient client = WebClient.create(vertx);
        int iterationCount = 15;
        AtomicInteger iterationIndex = new AtomicInteger(1);

        Action checkAccountBalancesAndTransactions = () -> {
            client.get(8080, "localhost", "/api/accounts")
                  .as(BodyCodec.string())
                  .send(testContext.succeeding(verifyBalances));
            client.get(8080, "localhost", "/api/transactions/" + 10)
                  .as(BodyCodec.string())
                  .send(testContext.succeeding(verifyTransactionExists));
            client.get(8080, "localhost", "/api/transactions/" + 11)
                  .as(BodyCodec.string())
                  .send(testContext.succeeding(verifyTransactionIsMissing));
        };

        VertxTestContext.ExecutionBlock createTransactions = () -> {
            Flowable<HttpResponse<Buffer>> allTransactions = Flowable.empty();
            for (int i = 0; i < iterationCount; i++) {
                allTransactions = Flowable.mergeArray(allTransactions,
                                                      client.post(8080, "localhost", "/api/transactions")
                                                            .rxSendJsonObject(newTx(1, 2, 10))
                                                            .toFlowable());
            }
            allTransactions.doOnComplete(checkAccountBalancesAndTransactions)
                           .subscribe();
        };

        //given
        createTestAccounts(client, testContext, createTransactions);
    }
}