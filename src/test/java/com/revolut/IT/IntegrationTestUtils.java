package com.revolut.IT;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxTestContext;
import io.vertx.reactivex.ext.web.client.HttpResponse;
import io.vertx.reactivex.ext.web.client.WebClient;
import io.vertx.reactivex.ext.web.codec.BodyCodec;
import lombok.experimental.UtilityClass;

@UtilityClass
class IntegrationTestUtils {

    static void createTestAccounts(WebClient client, VertxTestContext testContext, VertxTestContext.ExecutionBlock executeAfterCreation) {
        Handler<AsyncResult<HttpResponse<String>>> createSecondAccountAfterFirst = testContext.succeeding(response1 -> testContext.verify(() -> {
            Handler<AsyncResult<HttpResponse<String>>> createThirdAccountAfterSecond = testContext.succeeding(response2 -> testContext.verify(() -> {
                //#3
                client.post(8080, "localhost", "/api/accounts")
                      .as(BodyCodec.string())
                      .sendJsonObject(newAcc("acc3", 100), testContext.succeeding(response3 -> {
                          testContext.verify(executeAfterCreation);
                      }));
            }));
            //#2
            client.post(8080, "localhost", "/api/accounts")
                  .as(BodyCodec.string())
                  .sendJsonObject(newAcc("acc2", 100), createThirdAccountAfterSecond);
        }));
        //#1
        client.post(8080, "localhost", "/api/accounts")
              .as(BodyCodec.string())
              .sendJsonObject(newAcc("acc1", 100), createSecondAccountAfterFirst);
    }


    static JsonObject newAcc(String name, Number initialBalance) {
        return new JsonObject().put("name", name)
                               .put("balance", initialBalance);
    }

    static JsonObject newTx(Number from, Number to, Number amount) {
        return new JsonObject().put("accountFrom", from)
                               .put("accountTo", to)
                               .put("amount", amount);
    }
}