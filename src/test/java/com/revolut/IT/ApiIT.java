package com.revolut.IT;


import com.revolut.MainVerticle;
import io.reactivex.Completable;
import io.vertx.core.Handler;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.web.client.HttpResponse;
import io.vertx.reactivex.ext.web.client.WebClient;
import io.vertx.reactivex.ext.web.codec.BodyCodec;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.revolut.IT.IntegrationTestUtils.createTestAccounts;
import static com.revolut.IT.IntegrationTestUtils.newAcc;
import static com.revolut.IT.IntegrationTestUtils.newTx;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(VertxExtension.class)
class ApiIT {

    @BeforeEach
    void deployMainVerticle(Vertx vertx, VertxTestContext testContext) {
        vertx.deployVerticle(MainVerticle.class.getName(), testContext.completing());
    }

    @AfterEach
    @SneakyThrows
    void stopMainVerticle(Vertx vertx, VertxTestContext testContext) {
        vertx.deploymentIDs().stream()
             .map(vertx::rxUndeploy)
             .forEach(Completable::blockingAwait);
        vertx.close(testContext.completing());
    }

    @Test
    @DisplayName("Should create new account. " +
            "POST /api/accounts")
    void createAccount(Vertx vertx, VertxTestContext testContext) {
        //then
        Handler<HttpResponse<String>> verifyAccount = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("success");
            testContext.completeNow();
        });

        //when
        WebClient client = WebClient.create(vertx);
        client.post(8080, "localhost", "/api/accounts")
              .as(BodyCodec.string())
              .sendJsonObject(newAcc("acc1", 99.99),
                              testContext.succeeding(verifyAccount));
    }

    @Test
    @DisplayName("Should create an account and confirm its existence. " +
            "POST /api/accounts; GET /api/accounts/{accountId}")
    void createAccountAndCheckThatItExists(Vertx vertx, VertxTestContext testContext) {
        //then
        Handler<HttpResponse<String>> verifyAccount = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("acc1");
            assertThat(response.body()).contains("99.99");
            testContext.completeNow();
        });

        //when
        WebClient client = WebClient.create(vertx);
        VertxTestContext.ExecutionBlock getAccountAfterCreation = () ->
                client.get(8080, "localhost", "/api/accounts/1")
                      .as(BodyCodec.string())
                      .send(testContext.succeeding(verifyAccount));

        client.post(8080, "localhost", "/api/accounts")
              .as(BodyCodec.string())
              .sendJsonObject(newAcc("acc1", 99.99),
                              testContext.succeeding(response -> testContext.verify(getAccountAfterCreation)));
    }

    @Test
    @DisplayName("Should create multiple accounts and confirm that they exist. " +
            "POST /api/accounts; GET /api/accounts")
    void createMultipleAccountsAndCheckThatTheyExist(Vertx vertx, VertxTestContext testContext) {
        //then
        Handler<HttpResponse<String>> verifyAccounts = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("acc1");
            assertThat(response.body()).contains("acc2");
            assertThat(response.body()).contains("acc3");
            testContext.completeNow();
        });

        //when
        WebClient client = WebClient.create(vertx);
        VertxTestContext.ExecutionBlock getAccountsAfterCreation = () ->
                client.get(8080, "localhost", "/api/accounts")
                      .as(BodyCodec.string())
                      .send(testContext.succeeding(verifyAccounts));

        //given
        createTestAccounts(client, testContext, getAccountsAfterCreation);
    }

    @Test
    @DisplayName("Should create new transaction. " +
            "POST /api/accounts; POST /api/transactions")
    void createTransaction(Vertx vertx, VertxTestContext testContext) {
        //then
        Handler<HttpResponse<String>> verifySuccessfulCreation = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("success");
            testContext.completeNow();
        });

        //when
        WebClient client = WebClient.create(vertx);
        VertxTestContext.ExecutionBlock createNewTransaction = () ->
                client.post(8080, "localhost", "/api/transactions")
                      .as(BodyCodec.string())
                      .sendJsonObject(newTx(2, 1, 11),
                                      testContext.succeeding(verifySuccessfulCreation));

        //given
        createTestAccounts(client, testContext, createNewTransaction);
    }

    @Test
    @DisplayName("Should create a transaction and confirm its existence. " +
            "POST /api/accounts; POST /api/transactions; GET /api/transactions/{transactionId}")
    void createTransactionAndCheckThatItExists(Vertx vertx, VertxTestContext testContext) {
        //then
        Handler<HttpResponse<String>> verifyTransaction = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("\"accountFrom\":2");
            assertThat(response.body()).contains("\"accountTo\":1");
            assertThat(response.body()).contains("\"amount\":11.0");
            testContext.completeNow();
        });

        //when
        WebClient client = WebClient.create(vertx);
        VertxTestContext.ExecutionBlock getTransactionAfterCreation = () ->
                client.get(8080, "localhost", "/api/transactions/1")
                      .as(BodyCodec.string())
                      .send(testContext.succeeding(verifyTransaction));

        VertxTestContext.ExecutionBlock createNewTransaction = () ->
                client.post(8080, "localhost", "/api/transactions")
                      .as(BodyCodec.string())
                      .sendJsonObject(newTx(2, 1, 11),
                                      testContext.succeeding(response -> testContext.verify(getTransactionAfterCreation)));

        //given
        createTestAccounts(client, testContext, createNewTransaction);
    }

    @Test
    @DisplayName("Should create multiple transactions and confirm that they exist. " +
            "POST /api/accounts; POST /api/transactions; GET /api/transactions")
    void createMultipleTransactionsAndCheckThatTheyExist(Vertx vertx, VertxTestContext testContext) {
        //then
        Handler<HttpResponse<String>> verifyTransactions = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("\"id\":1,\"accountFrom\":2,\"accountTo\":1,\"amount\":11.0");
            assertThat(response.body()).contains("\"id\":2,\"accountFrom\":3,\"accountTo\":1,\"amount\":22.0");
            testContext.completeNow();
        });

        //when
        WebClient client = WebClient.create(vertx);
        VertxTestContext.ExecutionBlock getTransactions = () ->
                client.get(8080, "localhost", "/api/transactions")
                      .as(BodyCodec.string())
                      .send(testContext.succeeding(verifyTransactions));
        VertxTestContext.ExecutionBlock createSecondTransaction = () ->
                client.post(8080, "localhost", "/api/transactions")
                      .as(BodyCodec.string())
                      .sendJsonObject(newTx(3, 1, 22),
                                      testContext.succeeding(response -> testContext.verify(getTransactions)));
        VertxTestContext.ExecutionBlock createFirstTransaction = () ->
                client.post(8080, "localhost", "/api/transactions")
                      .as(BodyCodec.string())
                      .sendJsonObject(newTx(2, 1, 11),
                                      testContext.succeeding(response -> testContext.verify(createSecondTransaction)));

        //given
        createTestAccounts(client, testContext, createFirstTransaction);
    }

    @Test
    @DisplayName("Should create multiple transactions and confirm that they exist fetching by account. " +
            "POST /api/accounts; POST /api/transactions; GET /api/accounts/{accountId}/transactions")
    void createMultipleTransactionsAndCheckThatTheyExistFromAccountsPerspective(Vertx vertx, VertxTestContext testContext) {
        //then
        Handler<HttpResponse<String>> verifyTransactions = response -> testContext.verify(() -> {
            assertThat(response.body()).contains("\"id\":1,\"accountFrom\":2,\"accountTo\":1,\"amount\":11.0");
            assertThat(response.body()).contains("\"id\":2,\"accountFrom\":3,\"accountTo\":1,\"amount\":22.0");
            assertThat(response.body()).doesNotContain("\"amount\":33.0");
            testContext.completeNow();
        });

        //when
        WebClient client = WebClient.create(vertx);
        VertxTestContext.ExecutionBlock getAccountTransactions = () ->
                client.get(8080, "localhost", "/api/accounts/1/transactions")
                      .as(BodyCodec.string())
                      .send(testContext.succeeding(verifyTransactions));
        VertxTestContext.ExecutionBlock createThirdTransaction = () ->
                client.post(8080, "localhost", "/api/transactions")
                      .as(BodyCodec.string())
                      .sendJsonObject(newTx(3, 2, 33),
                                      testContext.succeeding(response -> testContext.verify(getAccountTransactions)));
        VertxTestContext.ExecutionBlock createSecondTransaction = () ->
                client.post(8080, "localhost", "/api/transactions")
                      .as(BodyCodec.string())
                      .sendJsonObject(newTx(3, 1, 22),
                                      testContext.succeeding(response -> testContext.verify(createThirdTransaction)));
        VertxTestContext.ExecutionBlock createFirstTransaction = () ->
                client.post(8080, "localhost", "/api/transactions")
                      .as(BodyCodec.string())
                      .sendJsonObject(newTx(2, 1, 11),
                                      testContext.succeeding(response -> testContext.verify(createSecondTransaction)));

        //given
        createTestAccounts(client, testContext, createFirstTransaction);
    }
}